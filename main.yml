# ansible_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

- name: Prepare, run experiment, retrieve results
  hosts: all
  gather_facts: true
  vars:
    base_dir: "{{ ansible_env.HOME }}/repo"
    results_dir: "{{ base_dir }}/results/{{ timestamp }}/{{ ansible_fqdn }}"
    ansible_python_interpreter: "/usr/bin/python3"
  tasks:
    - name: Prepare remote host before copying
      file:
        path: "{{ results_dir }}"
        state: directory
    - name: Copy repository to remote host
      synchronize:
        dest: "{{ ansible_env.HOME }}/repo"
        src: "{{ git_root }}/"
        rsync_opts:
          - "--exclude=results"
          - "--exclude=.git"
    - name: Setup experiment
      command:
        argv:
          - "{{ ansible_env.HOME }}/repo/guix-env-wrapper"
          - "{{ ansible_env.HOME }}/repo/setup/run-in-guix-env"
          - "{{ base_dir }}"
          - "{{ results_dir }}"
      args:
        creates: "{{ results_dir }}/setup.tar.xz"
    - name: Run experiment and handle errors
      block:
        - name: Run experiment
          command:
            argv:
              - "{{ ansible_env.HOME }}/repo/guix-env-wrapper"
              - "{{ ansible_env.HOME }}/repo/experiment/run-in-guix-env"
              - "{{ base_dir }}"
              - "{{ results_dir }}"
          register: out
          async: "{{ run_timeout }}"
          poll: 60
      always:
        - debug: var=out.stdout_lines
        - debug: var=out.stderr_lines
        - name: Teardown experiment
          command:
            argv:
              - "{{ ansible_env.HOME }}/repo/guix-env-wrapper"
              - "{{ ansible_env.HOME }}/repo/teardown/run-in-guix-env"
              - "{{ results_dir }}"
          args:
            creates: "{{ results_dir }}/teardown.tar.xz"
        - name: Create local results directory
          file:
            path: "{{ git_root }}/results/{{ timestamp }}"
            state: directory
          delegate_to: localhost
        - name: Retrieve results
          synchronize:
            mode: pull
            dest: "{{ git_root }}/results/{{ timestamp }}/"
            src: "{{ results_dir }}"

